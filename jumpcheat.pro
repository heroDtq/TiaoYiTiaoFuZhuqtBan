#-------------------------------------------------
#
# Project created by QtCreator 2018-01-04T09:34:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jumpcheat
TEMPLATE = app


SOURCES += main.cpp\
        cheatdlg.cpp

HEADERS  += cheatdlg.h

FORMS    += cheatdlg.ui
