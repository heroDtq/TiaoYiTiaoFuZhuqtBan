#ifndef CHEATDLG_H
#define CHEATDLG_H

#include <QDialog>

namespace Ui {
class CheatDlg;
}

class CheatDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CheatDlg(QWidget *parent = 0);
    ~CheatDlg();
    bool oneStep();
    int getPressTime(double iDistance);
private slots:
    void on_startbtn_clicked();

private:
    Ui::CheatDlg *ui;
};

#endif // CHEATDLG_H
